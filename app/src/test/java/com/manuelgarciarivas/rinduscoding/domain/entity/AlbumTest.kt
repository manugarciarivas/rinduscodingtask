package com.manuelgarciarivas.rinduscoding.domain.entity

import org.junit.Test

import org.junit.Assert.*

class AlbumTest {

    @Test
    fun getUserId() {
        val album : Album
        album = Album(1,1,"prueba")
        assertEquals(album.userId, 1)
    }

    @Test
    fun getId() {
        val album : Album
        album = Album(1,1,"prueba")
        assertEquals(album.id, 1)
    }

    @Test
    fun getTitle() {
        val album : Album
        album = Album(1,1,"prueba")
        assertEquals(album.title, "prueba")
    }
}