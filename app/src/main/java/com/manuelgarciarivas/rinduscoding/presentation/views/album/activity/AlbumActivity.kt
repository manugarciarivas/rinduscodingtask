package com.manuelgarciarivas.rinduscoding.presentation.views.album.activity

import android.os.Bundle
import android.view.View
import android.widget.TextView
import androidx.appcompat.app.AppCompatActivity
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProviders
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.manuelgarciarivas.rinduscoding.R
import com.manuelgarciarivas.rinduscoding.domain.entity.Album
import com.manuelgarciarivas.rinduscoding.presentation.views.album.viewmodel.AlbumViewModel
import de.rindus.mobiledevelopmentauxmoney.adapter.AlbumAdapter

class AlbumActivity : AppCompatActivity(){
    // Activity that show albums to the user.
    lateinit var albumViewModel: AlbumViewModel
    private val adapter = AlbumAdapter()
    lateinit var recyclerView: RecyclerView
    lateinit var tvError: TextView

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_album)

        initView()
        observeLiveData()
        loadData()
    }

    private fun initView(){
        recyclerView = findViewById<RecyclerView>(R.id.rv_album)
        tvError = findViewById<TextView>(R.id.tv_error)
        adapter.setOnItemClickListener(object : AlbumAdapter.ClickListener{
            override fun onClick(pos: Int, aView: View) {
                // empty method
            }
        })

        recyclerView.layoutManager = LinearLayoutManager(this)
        recyclerView.setHasFixedSize(true)
        recyclerView.adapter = adapter
    }

    private fun observeLiveData(){
        albumViewModel = ViewModelProviders.of(this).get(AlbumViewModel::class.java)
        albumViewModel.allAlbumsLivedata.observe(this, Observer<List<Album>> {
            if(it != null){
                adapter.setAlbums(it)
            }else{
                recyclerView.visibility = View.GONE
                tvError.visibility = View.VISIBLE
            }
        })
    }

    private fun loadData(){
        albumViewModel.loadAllAlbums()
    }
}