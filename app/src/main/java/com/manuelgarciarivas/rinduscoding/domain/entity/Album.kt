package com.manuelgarciarivas.rinduscoding.domain.entity

// Class with Album information
data class Album(
    val userId: Int,
    val id: Int,
    val title: String
)
