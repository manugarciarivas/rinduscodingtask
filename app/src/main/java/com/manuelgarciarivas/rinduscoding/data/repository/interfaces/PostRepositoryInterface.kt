package com.manuelgarciarivas.rinduscoding.data.repository.interfaces

interface PostRepositoryInterface {
    // This repository interface contains all calls to api that are about Posts

    fun getAllPosts()
    fun getPostsFromUser(userId: String)

}