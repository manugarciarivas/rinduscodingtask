package com.manuelgarciarivas.rinduscoding.presentation.views.posts.activity

import android.content.Intent
import android.os.Bundle
import android.view.View
import android.view.View.GONE
import android.view.View.VISIBLE
import android.widget.TextView
import androidx.appcompat.app.AppCompatActivity
import androidx.lifecycle.Observer
import com.manuelgarciarivas.rinduscoding.presentation.views.posts.viewmodel.PostsViewModel
import androidx.lifecycle.ViewModelProviders
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.manuelgarciarivas.rinduscoding.R
import com.manuelgarciarivas.rinduscoding.domain.entity.Post
import com.manuelgarciarivas.rinduscoding.presentation.views.comment.activity.CommentActivity
import de.rindus.mobiledevelopmentauxmoney.adapter.PostAdapter

class PostsActivity : AppCompatActivity(){
    // Activity that show posts to the user.
    lateinit var postsViewModel: PostsViewModel
    private val adapter = PostAdapter()
    lateinit var recyclerView: RecyclerView
    lateinit var tvError: TextView

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_posts)

        initView()
        observeLiveData()
        loadData()
    }

    private fun initView(){
        recyclerView = findViewById<RecyclerView>(R.id.rv_post)
        tvError = findViewById<TextView>(R.id.tv_error)
        adapter.setOnItemClickListener(object : PostAdapter.ClickListener{
            override fun onClick(pos: Int, aView: View) {
                val intent = Intent(this@PostsActivity, CommentActivity::class.java)
                intent.putExtra("postId", pos)
                startActivity(intent)
            }
        })

        recyclerView.layoutManager = LinearLayoutManager(this)
        recyclerView.setHasFixedSize(true)
        recyclerView.adapter = adapter
    }

    private fun observeLiveData(){
        postsViewModel = ViewModelProviders.of(this).get(PostsViewModel::class.java)
        postsViewModel.allPostsLiveData.observe(this, Observer<List<Post>> {
            if(it != null){
                adapter.setPosts(it)
            }else{
                recyclerView.visibility = GONE
                tvError.visibility = VISIBLE
            }
        })
    }

    private fun loadData(){
        postsViewModel.loadAllPosts()
    }
}