package com.manuelgarciarivas.rinduscoding.presentation.views.posts.viewmodel

import androidx.arch.core.executor.testing.InstantTaskExecutorRule
import androidx.test.ext.junit.runners.AndroidJUnit4
import com.manuelgarciarivas.rinduscoding.presentation.views.comment.viewmodel.CommentViewModel
import getOrAwaitValue
import org.junit.Assert.*
import org.junit.Rule
import org.junit.Test
import org.junit.runner.RunWith

@RunWith(AndroidJUnit4::class)
class PostsViewModelTest{
    @get:Rule
    var instantExecutorRule = InstantTaskExecutorRule()

    @Test
    fun loadAllPosts() {

        // Given a fresh ViewModel
        val postsViewModel = PostsViewModel()

        // When adding a new task
        postsViewModel.loadAllPosts()

        // Then the new task event is triggered
        val value = postsViewModel.allPostsLiveData.getOrAwaitValue()

        assertNotNull(value)
    }

    @Test
    fun loadPostsFromUser() {

        val postsViewModel = PostsViewModel()
        postsViewModel.loadPostsFromUser("1")
        val value = postsViewModel.postFromUserLiveData.getOrAwaitValue()

        assertNotNull(value)
    }

}