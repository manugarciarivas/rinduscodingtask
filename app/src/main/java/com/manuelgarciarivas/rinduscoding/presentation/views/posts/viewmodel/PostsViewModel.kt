package com.manuelgarciarivas.rinduscoding.presentation.views.posts.viewmodel

import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import com.manuelgarciarivas.rinduscoding.domain.entity.Post
import com.manuelgarciarivas.rinduscoding.domain.usecase.GetAllPostsUseCase
import com.manuelgarciarivas.rinduscoding.domain.usecase.GetPostsFromUserUseCase

class PostsViewModel : ViewModel() {
    // Viewmodel used by Activity to interact with service, livedata will be filled when service responds
    var allPostsLiveData : MutableLiveData<List<Post>>
    var postFromUserLiveData : MutableLiveData<List<Post>>

    init {
        allPostsLiveData = MutableLiveData()
        postFromUserLiveData = MutableLiveData()
    }

    fun loadAllPosts(){
        val getAllPostsUseCase: GetAllPostsUseCase
        getAllPostsUseCase = GetAllPostsUseCase(allPostsLiveData)
        getAllPostsUseCase.execute()
    }

    fun loadPostsFromUser(idUser: String){
        val getPostsFromUserUseCase: GetPostsFromUserUseCase
        getPostsFromUserUseCase = GetPostsFromUserUseCase(idUser,postFromUserLiveData)
        getPostsFromUserUseCase.execute()
    }
}