package com.manuelgarciarivas.rinduscoding.data.source

import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory

class ApiClient {
    // This class build the call to the endpoint and is used by repositories
    companion object{
        val BASE_URL ="https://jsonplaceholder.typicode.com"

        fun getApiClientInstance(): Retrofit{
            return Retrofit.Builder()
                .baseUrl(BASE_URL)
                .addConverterFactory(GsonConverterFactory.create())
                .build()
        }
    }
}