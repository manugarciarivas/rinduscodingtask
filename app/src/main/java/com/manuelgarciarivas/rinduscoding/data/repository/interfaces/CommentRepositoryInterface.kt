package com.manuelgarciarivas.rinduscoding.data.repository.interfaces

interface CommentRepositoryInterface {
    // This repository interface contains all calls to api that are about Comments

    fun getCommentFromPost(postId: Int)

}