package com.manuelgarciarivas.rinduscoding.domain.usecase

import androidx.lifecycle.MutableLiveData
import com.manuelgarciarivas.rinduscoding.data.repository.implementation.CommentRepository
import com.manuelgarciarivas.rinduscoding.domain.entity.Comment

class GetCommentFromPostUseCase constructor(val postId: Int, val liveData: MutableLiveData<List<Comment>>){
    // Usecase that call service that should return comments for a certain post

    fun execute(){
        val commentRepository: CommentRepository
        commentRepository = CommentRepository(liveData)
        commentRepository.getCommentFromPost(postId)
    }
}