package com.manuelgarciarivas.rinduscoding.domain.entity

// Class with Comment information
data class Comment(
    val postId: Int,
    val id: Int,
    val name: String,
    val email: String,
    val body: String
)
