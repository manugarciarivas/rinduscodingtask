package com.manuelgarciarivas.rinduscoding.presentation.views.album.viewmodel

import androidx.arch.core.executor.testing.InstantTaskExecutorRule
import androidx.test.ext.junit.runners.AndroidJUnit4
import getOrAwaitValue
import org.junit.Assert.*
import org.junit.Rule
import org.junit.Test
import org.junit.runner.RunWith

@RunWith(AndroidJUnit4::class)
class AlbumViewModelTest{
    @get:Rule
    var instantExecutorRule = InstantTaskExecutorRule()

    @Test
    fun loadAllAlbums() {

        val albumViewModel = AlbumViewModel()
        albumViewModel.loadAllAlbums()
        val value = albumViewModel.allAlbumsLivedata.getOrAwaitValue()

        assertNotNull(value)
    }
}