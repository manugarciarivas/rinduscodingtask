package de.rindus.mobiledevelopmentauxmoney.adapter

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView
import com.manuelgarciarivas.rinduscoding.R
import com.manuelgarciarivas.rinduscoding.domain.entity.Comment

class CommentAdapter : RecyclerView.Adapter<CommentAdapter.CommentHolder>() {
    // Adapter used to show albums inside recyclerview
    private var comments: List<Comment> = ArrayList()
    lateinit var mClickListener: ClickListener

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): CommentHolder {
        val itemView = LayoutInflater.from(parent.context)
            .inflate(R.layout.comment_item, parent, false)
        return CommentHolder(itemView)
    }

    override fun onBindViewHolder(holder: CommentHolder, position: Int) {
        val currentAlbum = comments[position]
        holder.commentUser.text = currentAlbum.email
        holder.commentTitle.text = currentAlbum.name
        holder.commentDescription.text = currentAlbum.body
    }

    override fun getItemCount(): Int {
        return comments.size
    }

    fun setComments(comments: List<Comment>) {
        this.comments = comments
        notifyDataSetChanged()
    }

    fun setOnItemClickListener(aClickListener: ClickListener) {
        mClickListener = aClickListener
    }

    interface ClickListener {
        fun onClick(pos: Int, aView: View)
    }

    inner class CommentHolder(itemView: View) : RecyclerView.ViewHolder(itemView), View.OnClickListener {
        var commentUser: TextView = itemView.findViewById(R.id.tv_comment_user)
        var commentTitle: TextView = itemView.findViewById(R.id.tv_comment_title)
        var commentDescription: TextView = itemView.findViewById(R.id.tv_comment_description)

        override fun onClick(v: View) {
            mClickListener.onClick(comments.get(adapterPosition).id, v)
        }

        init {
            itemView.setOnClickListener(this)
        }
    }
}