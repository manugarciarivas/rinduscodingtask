package com.manuelgarciarivas.rinduscoding.presentation.views.comment.viewmodel

import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import com.manuelgarciarivas.rinduscoding.domain.entity.Comment
import com.manuelgarciarivas.rinduscoding.domain.usecase.GetCommentFromPostUseCase

class CommentViewModel : ViewModel() {
    // Viewmodel used by Activity to interact with service, livedata will be filled when service responds
    var commentsFromPostLivedata : MutableLiveData<List<Comment>>

    init {
        commentsFromPostLivedata = MutableLiveData()
    }

    fun loadCommentFromPost(postId: Int){
        val getCommentFromPostUseCase: GetCommentFromPostUseCase
        getCommentFromPostUseCase = GetCommentFromPostUseCase(postId, commentsFromPostLivedata)
        getCommentFromPostUseCase.execute()
    }
}