package com.manuelgarciarivas.rinduscoding.presentation.views.album.viewmodel

import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import com.manuelgarciarivas.rinduscoding.domain.entity.Album
import com.manuelgarciarivas.rinduscoding.domain.usecase.GetAllAlbumUseCase

class AlbumViewModel : ViewModel() {
    // Viewmodel used by Activity to interact with service, livedata will be filled when service responds

    var allAlbumsLivedata : MutableLiveData<List<Album>>

    init {
        allAlbumsLivedata = MutableLiveData()
    }

    fun loadAllAlbums(){
        val getAllAlbumUseCase: GetAllAlbumUseCase
        getAllAlbumUseCase = GetAllAlbumUseCase(allAlbumsLivedata)
        getAllAlbumUseCase.execute()
    }
}