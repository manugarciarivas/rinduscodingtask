package com.manuelgarciarivas.rinduscoding.domain.entity

// Class with Post information
data class Post(
    val userId: Int,
    val id: Int,
    val title: String,
    val body: String
)
