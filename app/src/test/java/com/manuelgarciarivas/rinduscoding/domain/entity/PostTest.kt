package com.manuelgarciarivas.rinduscoding.domain.entity

import org.junit.Test

import org.junit.Assert.*

class PostTest {

    @Test
    fun getUserId() {
        val post : Post
        post = Post(1,1,"title","body")
        assertEquals(post.userId, 1)
    }

    @Test
    fun getId() {
        val post : Post
        post = Post(1,1,"title","body")
        assertEquals(post.id, 1)
    }

    @Test
    fun getTitle() {
        val post : Post
        post = Post(1,1,"title","body")
        assertEquals(post.title, "title")
    }

    @Test
    fun getBody() {
        val post : Post
        post = Post(1,1,"title","body")
        assertEquals(post.body, "body")
    }
}