package com.manuelgarciarivas.rinduscoding.domain.usecase

import androidx.lifecycle.MutableLiveData
import com.manuelgarciarivas.rinduscoding.data.repository.implementation.AlbumRepository
import com.manuelgarciarivas.rinduscoding.domain.entity.Album

class GetAllAlbumUseCase constructor(val liveData: MutableLiveData<List<Album>>){
    // Usecase that call service that should return all albums

    fun execute(){
        val albumRepository: AlbumRepository
        albumRepository = AlbumRepository(liveData)
        albumRepository.getAllAlbums()
    }
}