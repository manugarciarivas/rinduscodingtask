package com.manuelgarciarivas.rinduscoding.data.repository.interfaces

interface AlbumRepositoryInterface {
    // This repository interface contains all calls to api that are about Albums

    fun getAllAlbums()

}