package com.manuelgarciarivas.rinduscoding.domain.entity

import org.junit.Test

import org.junit.Assert.*

class CommentTest {

    @Test
    fun getPostId() {
        val comment : Comment
        comment = Comment(1,1,"prueba","email","body")
        assertEquals(comment.postId, 1)
    }

    @Test
    fun getId() {
        val comment : Comment
        comment = Comment(1,1,"prueba","email","body")
        assertEquals(comment.id, 1)
    }

    @Test
    fun getName() {
        val comment : Comment
        comment = Comment(1,1,"prueba","email","body")
        assertEquals(comment.name, "prueba")
    }

    @Test
    fun getEmail() {
        val comment : Comment
        comment = Comment(1,1,"prueba","email","body")
        assertEquals(comment.email, "email")
    }

    @Test
    fun getBody() {
        val comment : Comment
        comment = Comment(1,1,"prueba","email","body")
        assertEquals(comment.body, "body")
    }
}