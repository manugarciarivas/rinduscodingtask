package com.manuelgarciarivas.rinduscoding.data.repository.implementation

import androidx.lifecycle.MutableLiveData
import com.manuelgarciarivas.rinduscoding.data.repository.interfaces.PostRepositoryInterface
import com.manuelgarciarivas.rinduscoding.data.source.ApiClient
import com.manuelgarciarivas.rinduscoding.data.source.TypicodeApi
import com.manuelgarciarivas.rinduscoding.domain.entity.Post
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response

class PostRepository constructor(val liveData: MutableLiveData<List<Post>>): PostRepositoryInterface {
    // This repository contains all calls to api that are about Posts

    override fun getAllPosts() {
        val apiClient = ApiClient.getApiClientInstance().create(TypicodeApi::class.java)
        val call = apiClient.getAllPost()
        call.enqueue(object: Callback<List<Post>> {
            override fun onFailure(call: Call<List<Post>>, t: Throwable) {
                liveData.postValue(null)
            }

            override fun onResponse(call: Call<List<Post>>, response: Response<List<Post>>) {
                if(response.isSuccessful){
                    liveData.postValue(response.body())
                }else{
                    liveData.postValue(null)
                }
            }
        })
    }

    override fun getPostsFromUser(userId: String) {
        val apiClient = ApiClient.getApiClientInstance().create(TypicodeApi::class.java)
        val call = apiClient.getPostFromUser(userId)
        call.enqueue(object: Callback<List<Post>> {
            override fun onFailure(call: Call<List<Post>>, t: Throwable) {
                liveData.postValue(null)
            }

            override fun onResponse(call: Call<List<Post>>, response: Response<List<Post>>) {
                if(response.isSuccessful){
                    liveData.postValue(response.body())
                }else{
                    liveData.postValue(null)
                }
            }
        })
    }

}