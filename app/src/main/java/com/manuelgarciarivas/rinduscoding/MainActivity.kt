package com.manuelgarciarivas.rinduscoding

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.widget.Button
import com.manuelgarciarivas.rinduscoding.presentation.views.album.activity.AlbumActivity
import com.manuelgarciarivas.rinduscoding.presentation.views.posts.activity.PostsActivity

class MainActivity : AppCompatActivity() {
    // Main activity, just used to select what kind of data wants to see the user
    lateinit var buttonAlbum : Button
    lateinit var buttonPost : Button

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        initView()
    }

    private fun initView(){
        buttonAlbum = findViewById(R.id.button_option_album)
        buttonPost = findViewById(R.id.button_option_post)

        buttonAlbum.setOnClickListener {
            startActivity(Intent(this, AlbumActivity::class.java))
        }

        buttonPost.setOnClickListener {
            startActivity(Intent(this, PostsActivity::class.java))
        }
    }
}