package de.rindus.mobiledevelopmentauxmoney.adapter

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView
import com.manuelgarciarivas.rinduscoding.R
import com.manuelgarciarivas.rinduscoding.domain.entity.Album

class AlbumAdapter : RecyclerView.Adapter<AlbumAdapter.AlbumHolder>() {
    // Adapter used to show albums inside recyclerview
    private var albums: List<Album> = ArrayList()
    lateinit var mClickListener: ClickListener

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): AlbumHolder {
        val itemView = LayoutInflater.from(parent.context)
            .inflate(R.layout.album_item, parent, false)
        return AlbumHolder(itemView)
    }

    override fun onBindViewHolder(holder: AlbumHolder, position: Int) {
        val currentAlbum = albums[position]
        holder.albumTVTitle.text = currentAlbum.title
        holder.albumTVUser.text = currentAlbum.userId.toString()
    }

    override fun getItemCount(): Int {
        return albums.size
    }

    fun setAlbums(albums: List<Album>) {
        this.albums = albums
        notifyDataSetChanged()
    }

    fun setOnItemClickListener(aClickListener: ClickListener) {
        mClickListener = aClickListener
    }

    interface ClickListener {
        fun onClick(pos: Int, aView: View)
    }

    inner class AlbumHolder(itemView: View) : RecyclerView.ViewHolder(itemView), View.OnClickListener {
        var albumTVTitle: TextView = itemView.findViewById(R.id.tv_album_title)
        var albumTVUser: TextView = itemView.findViewById(R.id.tv_album_user)

        override fun onClick(v: View) {
            mClickListener.onClick(albums.get(adapterPosition).id, v)
        }

        init {
            itemView.setOnClickListener(this)
        }
    }
}