package com.manuelgarciarivas.rinduscoding.presentation.views.comment.activity

import android.os.Bundle
import android.view.View
import android.widget.TextView
import androidx.appcompat.app.AppCompatActivity
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProviders
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.manuelgarciarivas.rinduscoding.R
import com.manuelgarciarivas.rinduscoding.domain.entity.Comment
import com.manuelgarciarivas.rinduscoding.presentation.views.comment.viewmodel.CommentViewModel
import de.rindus.mobiledevelopmentauxmoney.adapter.CommentAdapter

class CommentActivity : AppCompatActivity(){
    // Activity that show comments to the user.
    lateinit var commentViewModel: CommentViewModel
    private val adapter = CommentAdapter()
    lateinit var recyclerView: RecyclerView
    lateinit var tvError: TextView
    var postId: Int = 0

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_comment)

        loadExtras()
        initView()
        observeLiveData()
        loadData()
    }

    private fun loadExtras(){
        postId = intent.getIntExtra("postId", 0)
    }

    private fun initView(){
        recyclerView = findViewById<RecyclerView>(R.id.rv_comment)
        tvError = findViewById<TextView>(R.id.tv_error)
        adapter.setOnItemClickListener(object : CommentAdapter.ClickListener{
            override fun onClick(pos: Int, aView: View) {
                // empty method
            }
        })

        recyclerView.layoutManager = LinearLayoutManager(this)
        recyclerView.setHasFixedSize(true)
        recyclerView.adapter = adapter
    }

    private fun observeLiveData(){
        commentViewModel = ViewModelProviders.of(this).get(CommentViewModel::class.java)
        commentViewModel.commentsFromPostLivedata.observe(this, Observer<List<Comment>> {
            if(it != null){
                adapter.setComments(it)
            }else{
                recyclerView.visibility = View.GONE
                tvError.visibility = View.VISIBLE
            }
        })
    }

    private fun loadData(){
        commentViewModel.loadCommentFromPost(postId)
    }
}