package com.manuelgarciarivas.rinduscoding.data.repository.implementation

import androidx.lifecycle.MutableLiveData
import com.manuelgarciarivas.rinduscoding.data.repository.interfaces.AlbumRepositoryInterface
import com.manuelgarciarivas.rinduscoding.data.source.ApiClient
import com.manuelgarciarivas.rinduscoding.data.source.TypicodeApi
import com.manuelgarciarivas.rinduscoding.domain.entity.Album
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response

class AlbumRepository constructor(val liveData: MutableLiveData<List<Album>>): AlbumRepositoryInterface {
    // This repository contains all calls to api that are about Albums

    override fun getAllAlbums() {
        val apiClient = ApiClient.getApiClientInstance().create(TypicodeApi::class.java)
        val call = apiClient.getAllAlbums()
        call.enqueue(object: Callback<List<Album>> {
            override fun onFailure(call: Call<List<Album>>, t: Throwable) {
                liveData.postValue(null)
            }

            override fun onResponse(call: Call<List<Album>>, response: Response<List<Album>>) {
                if(response.isSuccessful){
                    liveData.postValue(response.body())
                }else{
                    liveData.postValue(null)
                }
            }
        })
    }

}