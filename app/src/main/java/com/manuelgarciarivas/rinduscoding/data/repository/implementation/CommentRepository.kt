package com.manuelgarciarivas.rinduscoding.data.repository.implementation

import androidx.lifecycle.MutableLiveData
import com.manuelgarciarivas.rinduscoding.data.repository.interfaces.CommentRepositoryInterface
import com.manuelgarciarivas.rinduscoding.data.source.ApiClient
import com.manuelgarciarivas.rinduscoding.data.source.TypicodeApi
import com.manuelgarciarivas.rinduscoding.domain.entity.Comment
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response


class CommentRepository constructor(val liveData: MutableLiveData<List<Comment>>): CommentRepositoryInterface {
    // This repository contains all calls to api that are about Comments

    override fun getCommentFromPost(postId: Int) {
        val apiClient = ApiClient.getApiClientInstance().create(TypicodeApi::class.java)
        val call = apiClient.getCommentsFromPost(postId)
        call.enqueue(object: Callback<List<Comment>> {
            override fun onFailure(call: Call<List<Comment>>, t: Throwable) {
                liveData.postValue(null)
            }

            override fun onResponse(call: Call<List<Comment>>, response: Response<List<Comment>>) {
                if(response.isSuccessful){
                    liveData.postValue(response.body())
                }else{
                    liveData.postValue(null)
                }
            }
        })
    }

}