package com.manuelgarciarivas.rinduscoding.presentation.views.comment.viewmodel

import androidx.arch.core.executor.testing.InstantTaskExecutorRule
import androidx.test.ext.junit.runners.AndroidJUnit4
import getOrAwaitValue
import org.junit.Assert.*
import org.junit.Rule
import org.junit.Test
import org.junit.runner.RunWith

@RunWith(AndroidJUnit4::class)
class CommentViewModelTest{
    @get:Rule
    var instantExecutorRule = InstantTaskExecutorRule()

    @Test
    fun loadCommentFromPost() {

        val commentViewModel = CommentViewModel()
        commentViewModel.loadCommentFromPost(1)
        val value = commentViewModel.commentsFromPostLivedata.getOrAwaitValue()

        assertNotNull(value)
    }

}