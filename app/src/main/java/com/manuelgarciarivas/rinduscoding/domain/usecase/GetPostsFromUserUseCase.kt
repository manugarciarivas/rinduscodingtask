package com.manuelgarciarivas.rinduscoding.domain.usecase

import androidx.lifecycle.MutableLiveData
import com.manuelgarciarivas.rinduscoding.data.repository.implementation.PostRepository
import com.manuelgarciarivas.rinduscoding.domain.entity.Post

class GetPostsFromUserUseCase constructor(val userId: String, val liveData: MutableLiveData<List<Post>>){
    // Usecase that call service that should return post from certain user

    fun execute(){
        val postRepository: PostRepository
        postRepository = PostRepository(liveData)
        postRepository.getPostsFromUser(userId)
    }
}