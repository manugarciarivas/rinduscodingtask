package com.manuelgarciarivas.rinduscoding.data.source

import com.manuelgarciarivas.rinduscoding.domain.entity.*
import retrofit2.Call
import retrofit2.http.GET
import retrofit2.http.Query

interface TypicodeApi {
    // this interface contains all endpoints with information about parameters and response

    @GET("/posts")
    fun getAllPost(): Call<List<Post>>

    @GET("/posts")
    fun getPostFromUser(@Query("userId") userId: String): Call<List<Post>>

    @GET("/comments")
    fun getCommentsFromPost(@Query("postId") postId: Int): Call<List<Comment>>

    @GET("/albums")
    fun getAllAlbums(): Call<List<Album>>
}