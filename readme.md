This is a coding task made just for a test that consumes a REST API service from a fake API offered by https://jsonplaceholder.typicode.com/

You can import project using Android Studio, then you can compile and run it.

I have used MVVM pattern using Android Architecture components, in this way the UI has no dependency with taking information from API Rest.
So here we have an Activity, that request viewmodel about data information and just observe LiveData without blocking main thread.
Then, ViewModel executes the UseCase that it needs, this UseCase use the Repository to call service using an ApiClient. When the service
finnish the result will be set in the LiveData and in that moment the Activity will know that it has to update because the LiveData value
has changed and Activity is observing it.

In this project the more important thing was calling services and showing that information to the user, so i prefered not to save 
any information in the phone data, that optimizes the app data storage size. Also, in a real situation of "posts" and "comments", 
information could change every moment in the server so it's important to have information always updated. In this project the information 
is only text, so it's not so big loading from services. In case that app wants to optimize the calls to the service, it would be interesting
save information in the data phone storage, that would be possible using Room for example, and it could be an improvement.

There are a lot of new improvement possibles like: create more Tests, load more information from service, save information in the data phone
storage, create an user/pass system with encrypted password + an access token that after some minutes of inactivity inside app could expire and force user to login again.

This is all, thanks for your time and have a nice day :).
