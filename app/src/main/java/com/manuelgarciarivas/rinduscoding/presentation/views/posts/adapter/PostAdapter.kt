package de.rindus.mobiledevelopmentauxmoney.adapter

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView
import com.manuelgarciarivas.rinduscoding.R
import com.manuelgarciarivas.rinduscoding.domain.entity.Post

class PostAdapter : RecyclerView.Adapter<PostAdapter.PostHolder>() {
    // Adapter used to show albums inside recyclerview
    private var posts: List<Post> = ArrayList()
    lateinit var mClickListener: ClickListener

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): PostHolder {
        val itemView = LayoutInflater.from(parent.context)
            .inflate(R.layout.post_item, parent, false)
        return PostHolder(itemView)
    }

    override fun onBindViewHolder(holder: PostHolder, position: Int) {
        val currentPost = posts[position]
        holder.postTVTitle.text = currentPost.title
        holder.postTVBody.text = currentPost.body
        holder.postTVUser.text = currentPost.userId.toString()
    }

    override fun getItemCount(): Int {
        return posts.size
    }

    fun setPosts(posts: List<Post>) {
        this.posts = posts
        notifyDataSetChanged()
    }

    fun setOnItemClickListener(aClickListener: ClickListener) {
        mClickListener = aClickListener
    }

    interface ClickListener {
        fun onClick(pos: Int, aView: View)
    }

    inner class PostHolder(itemView: View) : RecyclerView.ViewHolder(itemView), View.OnClickListener {
        var postTVTitle: TextView = itemView.findViewById(R.id.tv_post_title)
        var postTVBody: TextView = itemView.findViewById(R.id.tv_post_body)
        var postTVUser: TextView = itemView.findViewById(R.id.tv_post_user)

        override fun onClick(v: View) {
            mClickListener.onClick(posts.get(adapterPosition).id, v)
        }

        init {
            itemView.setOnClickListener(this)
        }
    }
}